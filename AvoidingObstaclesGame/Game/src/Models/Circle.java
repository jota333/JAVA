package Models;

public class Circle extends Coordinate{
    
    private float radius;
    
    public Circle()
    {
        super();
        this.radius = 0;
    }
    public Circle(Coordinate nva, float r)
    {
        super(nva);
        this.radius = r; 
    }        
    public Circle(Circle cir)
    {
        super(cir);
        this.radius = cir.radius;
    }
    public float getRadius()
    {
        return this.radius;
    }
    public void setRadius(float r)
    {
        this.radius = r;
    }
    public Coordinate getCentro()
    {
        Coordinate coorCenter = new Coordinate(this.getX(), this.getY());
        return coorCenter;
    }
}
