package Models;

public class Coordinate {
    
    private float x;
    private float y;
    
    public Coordinate()
    {
       this.x = 0;
       this.y = 0;
    }
    public Coordinate(float x, float y)
    {
       this.x = x;
       this.y = y;
    }
    public Coordinate(Coordinate nva)
    {
        this.x = nva.x;
        this.y = nva.y;
    }
    
    public float getX()
    {
        return this.x;
    }
    public float getY()
    {
        return this.y;
    }
    
    public void setX(float x)
    {
        this.x = x;
    }
    
    public void setY(float y)
    {
        this.y = y;
    }
    
    
    public Coordinate Suma(Coordinate S)
    {
        float Sumx = this.x + S.getX();
        float Sumy = this.y + S.getY();
        
        Coordinate Cor = new Coordinate(Sumx,Sumy);
        
        return Cor;
    }
    
}
