package Models;

import java.awt.Color;
import java.awt.Graphics;

public class GraphicCircle extends Circle implements Drawable{
    
    Color color;
    
    public GraphicCircle(Coordinate cor, float radius, Color acolor)
    {
        super(cor, radius);
        this.color = acolor;
    }
    

    @Override
    public void draw(Graphics dw) {
        
        dw.setColor(color);
        dw.fillOval((int)(this.getX()-this.getRadius()),(int)(this.getY()-this.getRadius()),(int)(2*this.getRadius()),(int)(2*this.getRadius()));
    }   

    void pintar(Color a) 
    {
        this.color = a;
    }
    
}
