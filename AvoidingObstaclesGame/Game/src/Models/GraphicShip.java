package Models;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

public class GraphicShip extends Ship implements Drawable{
    
    Color color;
    
    public GraphicShip(Coordinate a,Coordinate b,Coordinate c, Color acolor)
    {
        super(a,b,c);
        this.color = acolor;
    }

    
    @Override
    public void draw(Graphics dw) {
        
        dw.setColor(color);
        int x[] = {(int)this.getX(),(int)this.cor1.getX(),(int)this.cor2.getX()};
        int y[] = {(int)this.getY(),(int)this.cor1.getY(),(int)this.cor2.getY()};
        
        //Para crear nuestranave triangulo debemos crear un poligono con nuestro arrego de x como parametro, arrerglo de y y la cantidad de lados =3
        Polygon p = new Polygon(x, y, 3);
        dw.fillPolygon(p);
    }
    
    public void Pintar(Graphics dw,Color acolor) {
        
        dw.setColor(acolor);
        int x[] = {(int)this.getX(),(int)this.cor1.getX(),(int)this.cor2.getX()};
        int y[] = {(int)this.getY(),(int)this.cor1.getY(),(int)this.cor2.getY()};
        
        //Para crear nuestranave triangulo debemos crear un poligono con nuestro arrego de x como parametro, arrerglo de y y la cantidad de lados =3
        Polygon p = new Polygon(x, y, 3);
        dw.fillPolygon(p);
    }
    
    public GraphicCircle Bala()
    {
        Coordinate salida = new Coordinate(this.getX(),this.getY());
        GraphicCircle bala = new GraphicCircle(salida, 10, Color.RED);
        return bala;
    }
    
    public void Ciclo()
    {
        for(int i=0; i<this.balas.size(); i++)
        {
            GraphicCircle y = (GraphicCircle) this.balas.get(i);
            float x = y.getY();
            y.setY(x -= 10);
        }
    }
    
    
}
