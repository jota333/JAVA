package Models;

public class Rectangle extends Coordinate {
    
    private float side1;
    private float side2;
    
    public Rectangle()
    {
        super();
        this.side1 = 0;
        this.side2 = 0;
    }
    
    public Rectangle(Coordinate cor, float x, float y)
    {
        super(cor);
        this.side1 = x;
        this.side2 = y;
    }
    
    public Rectangle(Rectangle nvo)
    {
        super(nvo.getX(),nvo.getY());
        this.side1 = nvo.side1;
        this.side2 = nvo.side2; 
        
    }
    
    public float getSide(int side)
    {
        if (side == 1) {
            
            return this.side1;
        }
        if (side == 2) {
            
            return this.side2;
        }
        if (side != 1 && side!= 2) {
            
            return 0;
            
        }
        return 0;
    }

}
