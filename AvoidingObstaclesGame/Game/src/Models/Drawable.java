package Models;

import java.awt.Color;
import java.awt.Graphics;

public interface Drawable {
    
    public void draw(Graphics dw);
    
}
