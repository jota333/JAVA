package Models;


import java.awt.Color;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;

public class Window extends JFrame implements WindowListener{
    
    public  Window(String title)
    {
        //Este super llama al constructor del padre que en este caso es JFrame, si sutua ahi y se fija cual de 
        //los constructores recibe un string por parametro y lo va a crear
        super(title);
        setSize(500,500);
        addWindowListener(this);
        setBackground(Color.yellow);
    }

    @Override
    public void windowOpened(WindowEvent e) {}

    @Override
    public void windowClosing(WindowEvent e) {System.exit(0);}

    @Override
    public void windowClosed(WindowEvent e) {}

    @Override
    public void windowIconified(WindowEvent e) {}

    @Override
    public void windowDeiconified(WindowEvent e) {}

    @Override
    public void windowActivated(WindowEvent e) {}

    @Override
    public void windowDeactivated(WindowEvent e) {}
    
}
