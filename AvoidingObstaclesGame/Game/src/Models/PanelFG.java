package Models;
import static Executions.main.Aleatorio;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import javax.swing.JPanel;

public class PanelFG extends JPanel implements KeyListener{
    
    ArrayList v;
    
    ArrayList ast = new ArrayList();
    
    GraphicShip nave;
    
    Coordinate MovIzq = new Coordinate(-25,0);
    Coordinate MovDer = new Coordinate(25,0);
    Coordinate MovNull = new Coordinate(0,0);
    Coordinate MovUp = new Coordinate(0,-25);
    Coordinate MovDown = new Coordinate(0,25);
    
    boolean FinDeJuego = true;
    
    int ContadorAsteroides =5;
    
    int Score;
    int Vidas = 3;
    
    
    TextoGrafico puntos;
    TextoGrafico vidas;
    TextoGrafico Final;
    
   
    
    
    public PanelFG(ArrayList vectordeO)
    {
        
        this.v = vectordeO;
        this.addKeyListener(this);
        setFocusable(true);
    }
    
    //Este metodo crea un objeto de tipo drawable y lo va dibujando a medida que recorre el arreglo de objetos
    @Override
    public void paint(Graphics g)
    {
        
        Dimension d = getSize();
        Image Imagen = createImage(d.width, d.height);
        Graphics buff = Imagen.getGraphics();
        
        Drawable drawable;
        for(int i=0; i<v.size();i++)
        {
            //casteamos esta lista de objetos cada uno al tipo dibujable para evitar error de compilacion
            drawable = (Drawable)v.get(i);
            drawable.draw(buff);
        }   
    
        g.drawImage(Imagen, 0, 0, null);
    }

    
    @Override
    public void update(Graphics g)
    {
        paint(g);
    }
    
    
    @Override
    public void keyTyped(KeyEvent e) {
        //Este no se usara pero lo dejamos 
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
        int tecla = e.getKeyCode();
        
        if (tecla == KeyEvent.VK_LEFT) 
        {
            this.nave.move(MovIzq);
            
        }
        if (tecla == KeyEvent.VK_RIGHT) 
        {
            
            this.nave.move(MovDer);
            
        }
        if (tecla == KeyEvent.VK_UP) 
        {
            
            this.nave.move(MovUp);
            
        }
        if (tecla == KeyEvent.VK_DOWN) 
        {
            
            this.nave.move(MovDown);
            
        }
        if (tecla == KeyEvent.VK_Q) 
        {
            GraphicCircle bala = nave.Bala();
            nave.balas.add(bala);
            v.add(bala);
            
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
        
        int tecla = e.getKeyCode();
        
        if (tecla == KeyEvent.VK_LEFT) 
        {
            
            this.nave.move(MovNull);
            
        }
        if (tecla == KeyEvent.VK_RIGHT) 
        {
            this.nave.move(MovNull);
        }
        if (tecla == KeyEvent.VK_UP) 
        {
            this.nave.move(MovNull);
        }
        if (tecla == KeyEvent.VK_DOWN) 
        {
            this.nave.move(MovNull);
        }
        
    }
    
    public void refNave (GraphicShip n)
    {
        this.nave = n;
    }
    public void refAst(GraphicRectangle a,GraphicRectangle b,
            GraphicRectangle c,GraphicRectangle d,GraphicRectangle e)
    {
        
        ast.add(a);
        ast.add(b);
        ast.add(c);
        ast.add(d);
        ast.add(e);
               
    }
    
    public void RefPuntos(TextoGrafico a)
    {
        this.puntos = a;
    }
    
    public void RefVidas(TextoGrafico b)
    {
        this.vidas = b;
    }
    
    public void RefFinal(TextoGrafico c)
    {
        this.Final = c;
    }
    
    
    
    
    public void Colision()
    {
        int i,j;
        for(i=0; i<nave.balas.size();i++)
        {
            GraphicCircle bala = (GraphicCircle)nave.balas.get(i);
            for(j=0; j<ast.size();j++)
            {
                GraphicRectangle aste = (GraphicRectangle)ast.get(j);
                
                Coordinate Corbala = new Coordinate(bala.getX(),bala.getY());
                Coordinate Derecha = new Coordinate(aste.getX()+30, aste.getY()); 
                Coordinate Izquierda = new Coordinate(aste.getX()-15, aste.getY());
                Coordinate medio = new Coordinate(aste.getX(), aste.getY()); 
                
                if(Corbala.getX() > Izquierda.getX() && Corbala.getX() < Derecha.getX()
                        &&Corbala.getY() < medio.getY() && Corbala.getY()+25 > medio.getY())
                {
                    aste.pintar(Color.yellow);  
                    bala.pintar(Color.yellow);
                    bala.setY(-100);
                    aste.setY(-100);
                    nave.balas.remove(bala); 
                    ast.remove(aste);
                    ContadorAsteroides--;
                    Score+=5;
                    puntos.setColor(Color.yellow);
                    String NuevoScore = ""+Score;
                    TextoGrafico Nrpuntos = new TextoGrafico(NuevoScore, Color.red, 1700, 350);
                    Nrpuntos.setSize(40);
                    puntos = Nrpuntos;
                    v.add(puntos);
                    
                    
                }
                
                if((medio.getY() > 475 && medio.getY() < 550 && (nave.cor1.getX() < medio.getX()) && 
                        nave.cor2.getX() > medio.getX()))
                {
                    Score = Score - 5;
                    Vidas--;
                    String NuevoScore = ""+Score;
                    String NuevasVidas = ""+Vidas;
                    
                    vidas.setColor(Color.yellow);
                    puntos.setColor(Color.yellow);
                    TextoGrafico Nrvidas = new TextoGrafico(NuevasVidas, Color.red, 1700, 150); 
                    Nrvidas.setSize(40);
                    vidas = Nrvidas;
                    
                    TextoGrafico NrPuntos = new TextoGrafico(NuevoScore, Color.red, 1700, 350);
                    NrPuntos.setSize(40);
                    puntos = NrPuntos;
                    
                    v.add(vidas);
                    v.add(puntos);
                    
                    ast.remove(aste);
                    aste.setY(2000);
                    ContadorAsteroides--;
                    
                    
                }
                
                
                
                
            }
        }
    }
    
    
    
    public void iniciar()
    {
        while (FinDeJuego) 
        {   
            try{
                if (!nave.balas.isEmpty())
                {   
                    nave.Ciclo();
                }
                
                
                for (int i = 0; i < ast.size(); i++) 
                {
                    GraphicRectangle rect = (GraphicRectangle) ast.get(i);
                    rect.Ciclo();
                    
                    
                    if (rect.getY() > 525) 
                    {
                        int rango = Aleatorio(800,50);
                        rect.setY(0);
                        rect.setX(rango );    
                    }
                
                }
                if(ContadorAsteroides<5)
                {
                    int rango = Aleatorio( 800, 50);
                    Coordinate inicio = new Coordinate(rango,0);
                    GraphicRectangle nuevo = new GraphicRectangle(inicio, 25, 25, Color.black);
                    ast.add(nuevo);
                    v.add(nuevo);
                    nuevo.Ciclo();
                    ContadorAsteroides++;
                    
                }
                
                if(Vidas <= 0)
                {
                    FinDeJuego = false;
                    v.add(Final);
                }
                Colision();
                
                
                
              
                
                Thread.sleep(50);
            }catch(InterruptedException err){System.out.println(err);}
            
            
            repaint();
        }
    }
}