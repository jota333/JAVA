package Models;

import java.awt.Color;
import java.awt.Graphics;

public class GraphicRectangle extends Rectangle implements Drawable{

    Color color;
    
    public GraphicRectangle(Coordinate cor, float x, float y, Color acolor)
    {
        super(cor,x,y);
        this.color = acolor;
    }
    
    @Override
    public void draw(Graphics dw) {
        
        dw.setColor(color);
        //Dibuja un rectangulo con relleno y debemos castear todos los parametroa a enteros
        dw.fillRect((int)this.getX(), (int)this.getY(), (int)this.getSide(1), (int)this.getSide(2));
    }
    
    public void Ciclo()
    {
        float x = this.getY();
        this.setY(x += 10);
         
    }
    public void pintar(Color a)
    {
        this.color = a;
    }
    
}
