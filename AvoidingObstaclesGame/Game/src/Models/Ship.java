package Models;

import java.util.ArrayList;

public class Ship extends Coordinate{
    
    //Al ser mi nave un traingulo hereda una coordenada del padre y debemos crear dos coordenadas mas al tratarse cada coordenada de un vertice del triangulo
    public Coordinate cor1 = new Coordinate(); //coordenada de la izquierda 
    public Coordinate cor2 = new Coordinate(); //coordenada de la derecha
    
    ArrayList balas = new ArrayList();
    
    //la coordernada que se hereda del padre toma a punta del triangulo
    public Ship()
    {
        super();
        
        this.cor1.setX(0);
        this.cor1.setY(0);
        
        this.cor2.setX(0);
        this.cor2.setY(0);    
    }
    
    public Ship(Coordinate a,Coordinate b,Coordinate c)
    {
        super(a.getX(),a.getY());
        
        this.cor1.setX(b.getX());
        this.cor1.setY(b.getY());
        
        this.cor2.setX(c.getX());
        this.cor2.setY(c.getY());
    }
    
    public Ship(Ship s)
    {
        super(s.getX(),s.getY());
        
        this.cor1.setX(s.getX());
        this.cor1.setY(s.getY());
        
        this.cor2.setX(s.getX());
        this.cor2.setY(s.getY());
    }
    
    //Para cuando quiera mover mi nave se debe setear los vertices para pintar de nuevo 
    public void setVertice(Coordinate nva,int side)
    {
        if (side ==1)//lado1 modifica la punta del triangulo 
        {
            this.setX(nva.getX());
            this.setY(nva.getY());
        }
        if (side ==2) 
        {
            this.cor1.setX(nva.getX());
            this.cor1.setY(nva.getY());
        }
        if (side ==3) 
        {
            this.cor2.setX(nva.getX());
            this.cor2.setY(nva.getY());
        }
    }
    
    
    public void move(Coordinate Ncoor)
    {
        setVertice((this.Suma(Ncoor)), 1); 
        setVertice((this.cor1.Suma(Ncoor)), 2); 
        setVertice((this.cor2.Suma(Ncoor)), 3); 
    }

    
    
}
