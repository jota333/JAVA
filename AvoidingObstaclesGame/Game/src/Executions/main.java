package Executions;
import Models.Coordinate;
import Models.GraphicRectangle;
import Models.GraphicShip;
import Models.PanelFG;
import Models.TextoGrafico;
import Models.Window;
import com.sun.istack.internal.FinalArrayList;
import java.awt.Color;

import java.util.ArrayList;

public class main {
    
    
    
   public static int Aleatorio(int Max, int Min)
   {
        return (int)(Math.random()*(Max-Min));
   }

   public static void main(String[] args) {
       
       Window gameWindow =  new Window("Avoid Obstacles Game");
       ArrayList objectsArray =  new ArrayList();
       
       
       //ship
       Coordinate cor3 = new Coordinate(475,500);//la punta
       Coordinate cor4 = new Coordinate(425,575);//esquina izquierda
       Coordinate cor5 = new Coordinate(525,575);//esquina derecha
       GraphicShip ship = new GraphicShip(cor3, cor4, cor5, Color.blue);
       
       //en el eje de las x queremos que caigan los cuadrados desde arriba por eso en x rango y en (y) -2 5
       
       
       TextoGrafico vidas = new TextoGrafico("Vidas", Color.blue, 1700, 50);
       vidas.setSize(35);
       objectsArray.add(vidas);
       
       TextoGrafico Score = new TextoGrafico("Score", Color.blue, 1700, 250);
       Score.setSize(35);
       objectsArray.add(Score);
       
       
       TextoGrafico Puntaje = new TextoGrafico("0", Color.red, 1700, 350);
       Puntaje.setSize(40);
       objectsArray.add(Puntaje);
       
       TextoGrafico Nrvidas = new TextoGrafico("3", Color.red, 1700, 150);
       Nrvidas.setSize(40);
       objectsArray.add(Nrvidas);
       
       TextoGrafico textofinal = new TextoGrafico("Final", Color.red, 900, 500);
       textofinal.setSize(40);
        
       
       //Creando los 5 asteorides
        
       int rango = Aleatorio( 800, 50);
       Coordinate Salida = new Coordinate(rango,0);
       GraphicRectangle Asteroide1 = new GraphicRectangle(Salida,25,25,Color.black);
       
       int rango1 = Aleatorio( 800, 50);
       Coordinate Salida1 = new Coordinate(rango1,0);
       GraphicRectangle Asteroide2 = new GraphicRectangle(Salida1,25,25,Color.black);
       
       int rango2 = Aleatorio( 800, 50);
       Coordinate Salida2 = new Coordinate(rango2,0);
       GraphicRectangle Asteroide3 = new GraphicRectangle(Salida2,25,25,Color.black);
       
       int rango3 = Aleatorio( 800, 50);
       Coordinate Salida3 = new Coordinate(rango3,0);
       GraphicRectangle Asteroide4 = new GraphicRectangle(Salida3,25,25,Color.black);
       
       int rango4 = Aleatorio( 800, 50);
       Coordinate Salida4 = new Coordinate(rango4,0);
       GraphicRectangle Asteroide5 = new GraphicRectangle(Salida4,25,25,Color.black);
       
       objectsArray.add(Asteroide1);
       objectsArray.add(Asteroide2);
       objectsArray.add(Asteroide3);
       objectsArray.add(Asteroide4);
       objectsArray.add(Asteroide5);
       
       objectsArray.add(ship);
       
       
       
       //Rectangle asteroid
       /*
       Coordinate cor1 = new Coordinate(250,250);
       GraphicRectangle rectangle = new GraphicRectangle(cor1, 60, 50, Color.black);
       objectsArray.add(rectangle);
       
       //Circle Comodin
       Coordinate cor2 = new Coordinate(350,350);
       GraphicCircle circle = new GraphicCircle(cor2, 20, Color.black);
       objectsArray.add(circle);
       */
       
       
    
       PanelFG panelFG = new PanelFG(objectsArray);
       
       
       panelFG.refNave(ship);
       panelFG.refAst(Asteroide1, Asteroide2, Asteroide3, Asteroide4, Asteroide5);
       
       panelFG.RefFinal(textofinal);
       panelFG.RefVidas(Nrvidas);
       panelFG.RefPuntos(Puntaje);
       
       
       
       gameWindow.add(panelFG);
       gameWindow.setSize(1000,600);
       gameWindow.setVisible(true);
       panelFG.iniciar(); 
       
       
    }

    
    
}
