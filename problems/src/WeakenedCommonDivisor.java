import java.util.ArrayList;
import java.util.List;

public class WeakenedCommonDivisor {

    public ArrayList<int[][]> listPairs;
    public ArrayList<Integer> currentDivisors;
    public ArrayList<Integer> nextDivisors;

    public WeakenedCommonDivisor(ArrayList<int[][]> listPairs) {
        this.listPairs = listPairs;
        currentDivisors = new ArrayList<>();
        nextDivisors = new ArrayList<>();
    }

    public void calculate(){
        ArrayList<Integer> coincidences = new ArrayList<>();
        if(listPairs.size() == 1) {
            listPairs.forEach( pair ->{
                currentDivisors = getFactorsList(pair[0][0]);
                nextDivisors = getFactorsList(pair[0][1]);
            });

            currentDivisors.forEach((factor) ->{
                if(nextDivisors.contains(factor)){
                    if(!coincidences.contains(factor)) {
                        coincidences.add(factor);
                    }
                }
            });
        } else {

            currentDivisors = getFactorsList(listPairs.get(0)[0][0]);
            getCurrentPosition(coincidences);


            if(coincidences.size() == 0) {
                currentDivisors.clear();
                currentDivisors = getFactorsList(listPairs.get(0)[0][1]);
                getCurrentPosition(coincidences);
            }
        }
        calculateTotal(coincidences);
    }

    public void getCurrentPosition(List<Integer> coincidences){
        int rowPosition = 0;
        for(int i=1; i<listPairs.size(); i++) {
            for (int j=0; j < 2; j++){
                nextDivisors = getFactorsList(listPairs.get(i)[rowPosition][j]);
                currentDivisors.forEach((factor) ->{
                    if(nextDivisors.contains(factor)){
                        if(!coincidences.contains(factor)) {
                            coincidences.add(factor);
                        }
                    }
                });
            }
            nextDivisors.clear();
            rowPosition = 0;
        }
    }

    public void calculateTotal(ArrayList<Integer> coincidences) {
         int res = 1;
         if(coincidences.size() == 0) {
             res = -1;
         }
         for (int i = 0; i < coincidences.size(); i++) {
            res *= coincidences.get(i);
         }
         System.out.println(res);
    }

    public  ArrayList<Integer> getFactorsList(int number){
        ArrayList<Integer> arrayList = new ArrayList<>();
        int counter = 2;
        while (counter <= number) {
            while (number%counter == 0) {
                number /= counter;
                if(!arrayList.contains(counter)) {
                    arrayList.add(counter);
                }
            }
                counter += 1;
        }
        return arrayList;
    }
}
