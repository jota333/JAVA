# CMSC350_Project1
Infix Expression Evaluator
- Given an infix expression, evaluate the expression using two stacks.
- Uses string tokenizer to properly split the expression onto their respective stacks.
- Basic GUI and exception handling for invalid input.
- Proper separation of front end and back end code.
