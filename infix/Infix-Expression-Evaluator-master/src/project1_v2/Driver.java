/**
 * Handles GUI, User Input, and Test Cases.
 * Relies on Evaluator.java to evaluate infix expressions.
*/

package project1_v2;

import java.awt.event.ActionEvent;
import javax.swing.*;


public class Driver 
{
    private JFrame mainFrame;
    private JPanel panel;
    private JLabel promptLabel;
    private JLabel resultLabel;
    private JTextField expressionField;
    private JTextField resultField;
    private JButton submitButton;

    
    
    /**
     * Instantiates and adds all GUI components to the JFrame.
     */
    private void prepareGUI() 
    {
        mainFrame = new JFrame("Infix Expression Evaluator");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        panel = (JPanel) mainFrame.getContentPane();
        mainFrame.setLayout(null);
        
        promptLabel = new JLabel ("Enter Infix Expression");
        promptLabel.setBounds(25, 5, promptLabel.getPreferredSize().width, promptLabel.getPreferredSize().height);
        panel.add(promptLabel);
        
        resultLabel = new JLabel ("Result");
        resultLabel.setBounds(50, 100, resultLabel.getPreferredSize().width, resultLabel.getPreferredSize().height); 
        panel.add(resultLabel);
        
        expressionField = new JTextField("", 20);
        expressionField.setBounds(175, 5, expressionField.getPreferredSize().width, expressionField.getPreferredSize().height);
        panel.add(expressionField);
        
        resultField = new JTextField("", 20);
        resultField.setBounds(125, 100, resultField.getPreferredSize().width, resultField.getPreferredSize().height);
        panel.add(resultField);
        
        submitButton = new JButton("Evaluate");
        submitButton.setBounds(200,50, submitButton.getPreferredSize().width, submitButton.getPreferredSize().height);
        
        submitButton.addActionListener((ActionEvent e) -> 
        {
            if(!expressionField.getText().equals(""))
            {
                evaluateButtonAction();
            }
        });
        panel.add(submitButton);
        
        mainFrame.setSize(500,175);
        mainFrame.setVisible(true);
    }
    
    
    
    /**
     * This function is fired when the Evaluate button is pressed.
     * It updates the result field to show what the expression was evaluated to.
     */
    private void evaluateButtonAction()
    {
        Evaluator  ev = new Evaluator();
        int result = ev.evaluate(expressionField.getText());
        resultField.setText("" + result);
    } 


    
    /**
     * Main function that launches the GUI and contains commented test cases.
     * @param args 
     */
    public static void main(String[] args) 
    {
        Driver driver = new Driver();
        driver.prepareGUI();
 
        //casos de prueba:
        /*
        Evaluator ev = new Evaluator();
        System.out.println(ev.Evaluate("(2*3) + (7/3)-3"));
        System.out.println(ev.Evaluate("1 - 9 * (4/2)"));
        System.out.println(ev.Evaluate("(9-3)*2+4"));
        System.out.println(ev.Evaluate("(2 + 3 * 5) -8/5 * (5-2)"));
        System.out.println(ev.Evaluate("4*5 + (7/ 0)"));
        System.out.println(ev.Evaluate("6 *8 - x"));
        */
    }
}