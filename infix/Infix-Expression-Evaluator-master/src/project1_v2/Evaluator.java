/**
 * Core logic for evaluating infix expressions.
 * Uses stacks to separate an expression into token and evaluate.
 * Precedence is taken into account so that arithmetic is correct.
 * 
 * Checks for division by zero and invalid characters.
 * 
*/
package project1_v2;

import java.util.Stack;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;


public class Evaluator 
{ 
    private final Stack<String> operatorStack;
    private final Stack<Integer> operandStack;
    
    
    
    /**
     * Constructor instantiates the two stacks
     */
    public Evaluator()
    {
        operatorStack = new Stack<String>();
        operandStack = new Stack<Integer>();
    }
 
    
    
    /**
     * This is the main logic for expression evaluation.
     * The expression is split into tokens and compared to see if they are operators or operands.
     * Operators use precedence to determine when a chunk of the expression should be evaluated.
     * @param expression
     * @return 
     */
    public int evaluate(String expression)
    { 
        StringTokenizer tokenizer = new StringTokenizer((expression.replaceAll("\\s+", "")), "()+-*/", true);

        while(tokenizer.hasMoreTokens())
        {
            String token = tokenizer.nextToken();
            
            if(token.matches("[0-9]+"))
            {
                operandStack.push(Integer.parseInt(token));
            }
            
            else if(token.equals("("))
            {
                operatorStack.push(token);
            }
            
            else if(token.equals(")"))
            {
                while(!operatorStack.peek().equals("("))
                {
                    operandStack.push(performOperation(operandStack.pop(),operandStack.pop(),operatorStack.pop()));
                }
                operatorStack.pop();
            }
            
            else if(isOperator(token))
            {
                while(!operatorStack.empty() && hasPrecedence(token,operatorStack.peek()) && operandStack.size() > 1)
                {
                    operandStack.push(performOperation(operandStack.pop(),operandStack.pop(),operatorStack.pop()));
                }
                operatorStack.push(token);
            }
            
            else{
                JOptionPane.showMessageDialog(null, "Invalid Character found in Expression.", "InvalidInput", JOptionPane.ERROR_MESSAGE);
                throw new IllegalArgumentException("Invalid character found in expression.");
            }
            //displayStacks(operatorStack, operandStack);   //DEBUGGING
        } 
        
        while(!operatorStack.empty() && operandStack.size() > 1)
        {
            operandStack.push(performOperation(operandStack.pop(),operandStack.pop(),operatorStack.pop()));
            //displayStacks(operatorStack, operandStack);   //DEBUGGING
        }
        
        System.out.println("\nOriginal Expression: " + expression);
        System.out.println("Final Result:  " + operandStack.peek());
        return operandStack.pop();
    }
    
    
    
    /**
     * Arithmetic is performed depending on the operator given.
     * Dividing By Zero throws an exception.
     * @param a The first operand to be in calculation.
     * @param b The second operand to be in calculation.
     * @param operator The operators include +,-,*,/
     * @return The result of the arithmetic is returned.
     */
    private int performOperation(int a, int b, String operator)
    {
        switch(operator)
        {
            case "+":
                return a + b;
            case "-":
                return Math.abs(a - b);     //Unsigned subtraction
            case "*":
                return a * b;
            case "/":
                if(a != 0)
                {
                    return b / a;
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "Cannot Divide by Zero.", "DivideByZero", JOptionPane.ERROR_MESSAGE);
                    throw new ArithmeticException("Cannot Divide by Zero");
                }
        }
        return -1;
    }
    
    
    
    /**
     * Return if a token is an operator {-,+,*,/}.
     * @param token Current token of expression
     * @return 
     */
    private boolean isOperator(String token)
    {
        return token.equals("+") || token.equals("-")|| token.equals("*") || token.equals("/");
    }
  
    
    
    /**
     * Returns if the first operator has higher precedence than the second operator.
     * @param a First operator.
     * @param b Second Operator.
     * @return 
     */
    private boolean hasPrecedence(String a, String b)
    {
        if (b.equals("(") || b.equals(")"))
        {
            return false;
        }
        return !((a.equals("*") || a.equals("/")) && (b.equals("+") || b.equals("-")));
    }
    
    
    
    /**
     * Used to easily view what each stack contained in each loop iteration
     * @param operators OperatorStack
     * @param values OperandStack
     */
    private void displayStacks(Stack<String> operators, Stack<Integer> values)
    {
        System.out.println("Operator Stack --- " + operators);
        System.out.println("Operand Stack ---- " + values);
        System.out.println("---------------------------------------------\n");
    } 
}