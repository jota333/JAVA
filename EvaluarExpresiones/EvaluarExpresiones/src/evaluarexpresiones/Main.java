
package evaluarexpresiones;

public class Main extends javax.swing.JFrame {
        
    public Main() {
        initComponents();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblExpresion = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtExpresion = new javax.swing.JTextArea();
        btnEvaluar = new javax.swing.JButton();
        lblResultado = new javax.swing.JLabel();
        txtResultado = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Evaluador de Expresiones Matemáticas");
        setAlwaysOnTop(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblExpresion.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        lblExpresion.setText("Expresión Matemática");
        getContentPane().add(lblExpresion, new org.netbeans.lib.awtextra.AbsoluteConstraints(36, 14, -1, -1));

        jScrollPane1.setViewportView(txtExpresion);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(38, 30, 184, 62));

        btnEvaluar.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        btnEvaluar.setText("Evaluar");
        btnEvaluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEvaluarActionPerformed(evt);
            }
        });
        getContentPane().add(btnEvaluar, new org.netbeans.lib.awtextra.AbsoluteConstraints(226, 30, 74, -1));

        lblResultado.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        lblResultado.setText("Resultado");
        getContentPane().add(lblResultado, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 98, -1, -1));

        txtResultado.setEditable(false);
        getContentPane().add(txtResultado, new org.netbeans.lib.awtextra.AbsoluteConstraints(42, 114, 180, -1));

        jLabel1.setText("linear equation input");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 170, -1, -1));

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        getContentPane().add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 200, 170, -1));

        setSize(new java.awt.Dimension(675, 447));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnEvaluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEvaluarActionPerformed
        try {
            evaluar();            
        } catch (Exception e) {
            javax.swing.JOptionPane.showMessageDialog(this, e.getMessage());
        }        
    }//GEN-LAST:event_btnEvaluarActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed
    
    private void evaluar() throws Exception {
        Calc calc = new Calc();
        String exp = txtExpresion.getText();
        txtResultado.setText( String.valueOf(calc.calc(exp)) );
    }
    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEvaluar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel lblExpresion;
    private javax.swing.JLabel lblResultado;
    private javax.swing.JTextArea txtExpresion;
    private javax.swing.JTextField txtResultado;
    // End of variables declaration//GEN-END:variables
    
}
