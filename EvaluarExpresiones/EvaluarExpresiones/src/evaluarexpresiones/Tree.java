package evaluarexpresiones;

public abstract class Tree {  
    public static final int ERROR=0, ID=1, BRACKET=2, ADD=3, SUB=4, MUL=5, DIV=6, POW=7;

   /** 
    * Build a tree with one subtree, given its kind tag
    */
    static Tree build1(int kind, Tree node) {  
        if (kind != BRACKET) {  
           System.out.println("Internal error: wrong kind of tree");
            System.exit(1);
        }
        return new Bracket(node);
    }

   
   public static Tree build2(int kind, Tree node1, Tree node2) {  
       switch (kind) {
            case ADD:  return new Add(node1, node2);
            case SUB:  return new Sub(node1, node2);
            case MUL:  return new Mul(node1, node2);
            case DIV:  return new Div(node1, node2);
            case POW:  return new Pow(node1, node2);
            default:
                System.out.println("Internal error: wrong kind of tree");
                System.exit(1);
        }
        return null;
   }

   
   abstract int getKind();
   abstract int getPrefix();
   abstract int getPostfix();

   
    public static class Error extends Tree {  
        String message;
        int start, end;
        Error(int s, int e) { start = s; end = e; }
        int getKind() { 
            return ERROR; 
        }
        int getPrefix() { 
            return 0; 
        }
        int getPostfix() {
            return 0; 
        }
    }

    
    public static class Id extends Tree {  
        int ref, start;
        Id(int r, int s) { 
            ref = r; start = s; 
        }
        int getKind() { 
            return ID; 
        }
        int getPrefix() { 
            return 0; 
        }
        int getPostfix() { 
            return 0; 
        }
    }

    
    public static class Bracket extends Tree {  Tree expr;
        Bracket(Tree e) { 
            expr = e; 
        }
        int getKind() { 
            return BRACKET; 
        }
        int getPrefix() { 
            return 1; 
        }
        int getPostfix() { 
            return 1; 
        }
    }

   
    public static class Add extends Tree {  
        Tree left, right;
        Add(Tree l, Tree r) { 
            left = l; right = r; 
        }
        int getKind() { 
            return ADD; 
        }
        int getPrefix() { 
            return 0; 
        }
        int getPostfix() { 
            return 0; 
        }
    }

    public static class Sub extends Tree {  
        Tree left, right;
        Sub(Tree l, Tree r) { 
            left = l; right = r; 
        }
        int getKind() { 
            return SUB; 
        }
        int getPrefix() { 
            return 0; 
        }
        int getPostfix() { 
            return 0; 
        }
    }

    public static class Mul extends Tree {  
        Tree left, right;
        Mul(Tree l, Tree r) { 
            left = l; right = r; 
        }
        int getKind() { 
            return MUL; 
        }
        int getPrefix() { 
            return 0; 
        }
        int getPostfix() { 
            return 0; 
        }
    }

    public static class Div extends Tree {  
        Tree left, right;
        Div(Tree l, Tree r) { 
            left = l; right = r; 
        }
        int getKind() { 
            return DIV; 
        }
        int getPrefix() { 
            return 0; 
        }
        int getPostfix() { 
            return 0; 
        }
    }

    public static class Pow extends Tree {  
        Tree left, right;
        Pow(Tree l, Tree r) { 
            left = l; right = r; 
        }
        int getKind() { 
            return POW; 
        }
        int getPrefix() { 
            return 0; 
        }
        int getPostfix() { 
            return 0; 
        }
    }
}
