

package evaluarexpresiones;

public abstract class Rule {  
    public static final int EMPTY=0, SKIP=1, ACCEPT=2, BUILD=3, THEN=4, OR=5;
    abstract int getKind();

    
    public static class Empty extends Rule {  
        Empty() { }
        int getKind() { 
            return EMPTY; 
        }
    }

   
    public static class Skip extends Rule {  
        int symbolKind;
        Skip(int k) { 
            symbolKind = k; 
        }
        int getKind() { 
            return SKIP; 
        }
    }

    
    public static class Accept extends Rule {  
        int symbolKind;
        Accept(int k) { 
            symbolKind = k; 
        }
        int getKind() { 
            return ACCEPT; 
        }
    }

   
    public static class Build extends Rule {  
        int kind, size;
        Build(int k, int n) { 
            kind = k; size = n; 
        }
        int getKind() { 
            return BUILD; 
        }
    }

   
    
    public static class Then extends Rule {  
        int left, right;
        Then(int l, int r) { 
            left = l; right = r; 
        }
        int getKind() { 
            return THEN; 
        }
    }

   
    public static class Or extends Rule {  
        int left, right;
        Or(int l, int r) { 
            left = l; right = r; 
        }
        int getKind() { 
            return OR; 
        }
    }
}